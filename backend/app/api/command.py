from app import utils
from flask import current_app

COMMANDS = {
    # tcos-desktop-populate must be run as user tcos with a correct HOME in env.
    # Model data will be requested (via tcos_settings) from this very same
    # single-threaded web server which can only happen after the current request
    # has been processed.
    'desktop-reload': 'nohup runuser -l tcos -c "tcos-desktop-populate -r reload --no-autorun" 2&> /dev/null &'
}

def post(command):
    if command not in COMMANDS:
        current_app.logger.error('Unkown command {command}'.format(**locals()))
        return NoContent, 404
    else:
        command = COMMANDS[command]

    current_app.logger.info('Running command: {command}'.format(**locals()))
    output, exit_code = utils.run_command(command)
    result = {
        'code': exit_code,
        'output': '\n'.join(output).strip()
    }
    return result, exit_code and 500 or 200
