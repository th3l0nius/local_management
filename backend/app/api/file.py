#!/usr/bin/env python

from app.config import file_types
from app import utils
from flask import current_app
from connexion import NoContent
from flask import Response
import XpmImagePlugin
from PIL import Image, UnidentifiedImageError
import shutil
import magic
import os


def search(type, name, size=None):
    if type not in file_types:
        current_app.logger.error('Uknown filetype: {type}'.format(**locals()))
        return NoContent, 404

    file_path = os.path.join(file_types[type]['path'], name)
    if not os.path.isfile(file_path):
        current_app.logger.error('File not found: {file_path}'.format(**locals()))
        return NoContent, 404

    def send_file(path):
        with open(path, 'rb') as file:
            file_content = file.read()
            mime_type = magic.from_buffer(file_content, mime=True)
            if mime_type == 'image/svg':
                # Firefox does not display image/svg (i.e. SVG w/o XML header)
                # but has no problem rendering the same file as image/svg+xml
                mime_type = 'image/svg+xml'
            return (Response(file_content, mimetype=mime_type), 200)

    if size is None:
        return send_file(file_path)
    else:
        thumbs_path = os.path.join(file_types[type]['thumbs'], name)
        thumbnail_path = os.path.join(thumbs_path, size + '.png')
        if not os.path.isfile(thumbnail_path):
            try:
                image = Image.open(file_path)
                image.thumbnail([*map(int, size.split('x'))])
                if not os.path.exists(thumbs_path):
                    os.makedirs(thumbs_path)
                image.save(thumbnail_path)
            except Exception as ex:
                return send_file(file_path)
        return send_file(thumbnail_path)


def put(type, name, body):
    if type not in file_types:
        return NoContent, 404
    os.makedirs(file_types[type]['path'], exist_ok=True)
    file_path = os.path.join(file_types[type]['path'], name)
    thumbs_path = os.path.join(file_types[type]['thumbs'], name)
    with open(file_path, 'bw') as file:
        file.write(body)
        shutil.rmtree(thumbs_path, ignore_errors=True)
    return NoContent, 204


def delete(type, name):
    if type not in file_types:
        return NoContent, 404
    file_path = os.path.join(file_types[type]['path'], name)
    thumbs_path = os.path.join(file_types[type]['thumbs'], name)
    if os.path.isfile(file_path):
        os.unlink(file_path)
        shutil.rmtree(thumbs_path, ignore_errors=True)
        if type == 'wallpaper':
            default_file_path = os.path.join(file_types[type]['path'], 'default.png')
            utils.run_command('cp {default_file_path} {file_path}'.format(**locals()))

        return NoContent, 204
    else:
        return NoContent, 404
