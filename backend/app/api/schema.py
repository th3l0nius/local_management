#!/usr/bin/env python

import json
from flask import current_app, Response


def search(type=None, subtype=None):
    results = list()
    for key in current_app.json_schemas:
        if type and subtype:
            if current_app.json_schemas[key]['type'] == type and current_app.json_schemas[key]['subtype'] == subtype:
                results.append(current_app.json_schemas[key])
        elif type and not subtype:
            if current_app.json_schemas[key]['type'] == type:
                results.append(current_app.json_schemas[key])
        elif not type and subtype:
            if current_app.json_schemas[key]['subtype'] == subtype:
                results.append(current_app.json_schemas[key])
        else:
            results.append(current_app.json_schemas[key])

    return Response(json.dumps(results), mimetype='application/json'), 200
