#!/usr/bin/env python

from app import utils
import app.api.schema
from flask import current_app
import json


# Temporary workaround for illegal hyphens in some setting names.
# This will be fixed in the schema files in the near future.
def encode_hyphen_in_column_names(settings):
    return {k.replace('-', '___'): v for k,v in settings.items()}


def decode_hyphen_in_column_names(settings):
    return {k.replace('___', '-'): v for k,v in settings.items()}


def search(id, with_defaults=False, flattened_settings=False):
    # get database connection
    db = utils.get_db()
    # check if item with it exists
    table_shortitems = db['shortitems']
    item = table_shortitems.find_one(id=id)
    if not item:
        return None, 204

    # get settings table for item
    if item.get('subtype'):
        table_settings = db['settings.{type}.{subtype}'.format(**item)]
    else:
        table_settings = db['settings.{type}'.format(**item)]

    settings = decode_hyphen_in_column_names(table_settings.find_one(item_id=id))

    # Load default settings from JSON Schema
    if with_defaults:
        current_app.logger.info('Loading default values from JSON Schema'.format(**locals()))
        # Get JSON schema
        response, http_code = app.api.schema.search(type=item['type'], subtype=item['subtype'])
        json_schema = json.loads(response.get_data(as_text=True))[0]
        # Get default settings
        default_settings = utils.get_defaults(json_schema)
        
        # Update settings with default settings
        default_settings.update(settings)
        settings = default_settings
    else:
        current_app.logger.info('Not loading default values from JSON Schema'.format(**locals()))
        
    if flattened_settings:
        item['settings'] = settings
    else:
        item['settings'] = utils.deflatten_dict(settings)
    

    return item, 200


def post(body):
    db = utils.get_db()
    db.begin()
    try:
        table_shortitems = db['shortitems']

        # get settings table for item
        if body.get('subtype'):
            table_settings = db['settings.{type}.{subtype}'.format(**body)]
        else:
            table_settings = db['settings.{type}'.format(**body)]

        settings = body.pop('settings', dict())

        # Drop id when creating new item
        body.pop('id', None)
        
        # Update shortitems table
        item_id = table_shortitems.insert(body)
        body['id'] = item_id

        # Update settings table
        settings.pop('id', None)
        settings['item_id'] = item_id
        table_settings.insert(encode_hyphen_in_column_names(utils.flatten_dict(settings)))
        db.commit()
    except Exception as e:
        db.rollback()
        raise e

    return dict(id=item_id), 200


def put(id, body):
    # get database connection
    db = utils.get_db()
    # check if item with it exists
    table_shortitems = db['shortitems']
    item = table_shortitems.find_one(id=id)
    if not item:
        return None, 404

    # get settings table for item
    if body.get('subtype'):
        table_settings = db['settings.{type}.{subtype}'.format(**body)]
    else:
        table_settings = db['settings.{type}'.format(**body)]

    # Update settings table
    db.begin()
    try:
        settings = table_settings.find_one(item_id=id)
        settings.update(body['settings'])
        table_settings.update(encode_hyphen_in_column_names(utils.flatten_dict(settings)), ['item_id'])

        # Update shortitems table
        del body['settings']
        body['id'] = id
        table_shortitems.upsert(body, ['id'])

        db.commit()
    except Exception as e:
        db.rollback()
        raise e

    return dict(id=id), 200


def delete(id):
    # get database connection
    db = utils.get_db()
    # check if item with it exists
    table_shortitems = db['shortitems']
    item = table_shortitems.find_one(id=id)
    if not item:
        return None, 404

    table_relationships = db['relationships']

    if item.get('subtype'):
        table_settings = db['settings.{type}.{subtype}'.format(**item)]
    else:
        table_settings = db['settings.{type}'.format(**item)]

    table_settings.delete(item_id=id)
    table_shortitems.delete(id=id)
    table_relationships.delete(left_id=id)
    table_relationships.delete(right_id=id)

    return None, 204
