#!/usr/bin/env python

import app.api.license
from app import utils
from datetime import datetime

def _reset_config():
	""" Reset config and first boot date """
	today = datetime.strftime(datetime.now(), '%Y-%m-%d')
	utils.run_command('mv /config/db.sqlite /config/db.sqlite.{today}'.format(**locals()))
	utils.run_command('cp /run/initramfs/live/config/db.sqlite /config/db.sqlite')
	utils.run_command('mount -o remount,rw /boot')
	utils.run_command('grub-editenv /boot/boot/grub/grubenv set firstboot={today}'.format(**locals()))
	utils.run_command('mount -o remount,ro /boot')
	utils.init_db()


def search():
	result = {
		'popupInterval': 0,
		'popupTextEn': '',
		'popupTextDe': '',
		'valid': True
	}

	license, http_code = app.api.license.search()
	if not license:
		pass
	elif license.get('type') == 'evaluation' and license.get('valid'):
		remaining_days = license['remaining_days']
		result = {
			'popupInterval': 1,
			'popupTextEn': '''
				<h4>Evaluation license</h4>
				<p>{remaining_days} days remaining of the evaluation license.</p>
				<p>After this period all you have to provide a valid license.</p>
				'''.format(**locals()),
			'popupTextDe': '''
				<h4>Evaluationslizenz</h4>
				<p>Es bleiben noch {remaining_days} Tage vor dem Ablauf des Testzeitraums.</p>
				<p>Nach Ablauf der Evaluationslizenz müssen Sie eine gültige Lizenz PIN angeben. </p>
				'''.format(**locals()),
			'valid': True,
		}
	elif license.get('type') == 'evaluation' and not license.get('valid'):
		result = {
			'popupInterval': 1,
			'popupTextEn': '''
				<h4>Evaluation license</h4>
				<p><b>The evaluation license expired.</b></p>
				<p>Please buy a usage license: https://openthinclient.com</p>
				'''.format(**locals()),
			'popupTextDe': '''
				<h4>Evaluationslizenz</h4>
				<p><b>Der Testzeitraum ist abgelaufen.</b></p>
				<p>Kaufen Sie bitte eine Nutzungslizenz unter http://shop.openthinclient.com.</p>
				'''.format(**locals()),
			'valid': False,
		}

	elif license.get('type') == 'full' and license.get('valid') and license.get('grace_period'):
		remaining_days = license['remaining_days']
		result = {
			'popupInterval': 1,
			'popupTextEn': '''
				<h4>License expired!</h4>
				<p>
					Please buy a new usage license from http://shop.openthinclient.com within
					{remaining_days} days.
				</p>
				'''.format(**locals()),
			'popupTextDe': '''
				<h4>Lizenz abgelaufen!</h4>
				<p>
					Kaufen Sie bitte innerhalb der nächsten {remaining_days} Tage 
					eine neue Nutzungslizenz unter http://shop.openthinclient.com.
				</p>
				'''.format(**locals()),
			'valid': True,
		}
	elif license.get('type') == 'full' and not license.get('valid'):
		result = {
			'popupInterval': 1,
			'popupTextEn': '''
			<h4>License expired!</h4>
			<p>Please buy a new usage license: https://openthinclient.com</p>
			'''.format(**locals()),
			'popupTextDe': '''
			<h4>Lizenz abgelaufen!</h4>
			<p>Kaufen Sie bitte eine neue Nutzungslizenz unter https://openthinclient.com.</p>
			'''.format(**locals()),
			'valid': False,
		}

	return result, 200

