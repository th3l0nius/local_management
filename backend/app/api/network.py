#!/usr/bin/env python

from app import utils
import app.api.schema
from flask import current_app
from connexion import NoContent
import json
import re
import configparser
import subprocess
import tempfile
import time
import os


def search():
	command = 'ip -j addr show'
	attempts = 6
	for trial in range(attempts):
		ip_output, exit_code = utils.run_command(command)
		ip_output = '\n'.join(ip_output)
		if ip_output.strip():
			break
		time.sleep(0.5)
		
	if exit_code != 0:
		error = {
			'code': exit_code,
			'message': {
				'de': '"{command}" ist mit folgender Meldung gescheitert: {ip_output}'.format(**locals()),
				'en': '"{command}" failed with the following result: {ip_output}'.format(**locals()),
			}
		}
		return error, 500
	try:
		interfaces = json.loads(ip_output)
	except json.decoder.JSONDecodeError as e:
		error = {
			'code': 500,
			'message': {
				'de': 'JSON Decoder ist gescheitert: {e}'.format(**locals()),
				'en': 'JSON Decoder failed: {e}'.format(**locals()),
			}
		}
		return error, 500


	config = dict()
	for interface in interfaces:
		name = interface["ifname"]
		command = 'networkctl status {name}'.format(**locals())
		vendor = model = network_file = state = gateway = ip = netmask = type = None
		dns = list()
		try:
			networkctl_output = str(subprocess.check_output(command, shell=True))
		except subprocess.CalledProcessError as e:
			error = {
				'code': 1,
				'message': {
					'de': '"{command}" ist mit folgender Meldung gescheitert: {networkctl_output}'.format(**locals()),
					'en': '"{command}" failed with the following result: {networkctl_output}'.format(**locals()),
				}
			}
			return error, 500
		# Get Vendor
		try:
			vendor = re.match(r'.*Vendor: (.*?)\\n', networkctl_output, re.MULTILINE).group(1)
		except AttributeError:
			vendor = None
		# Get Model
		try:
			model = re.match(r'.*Model: (.*?)\\n', networkctl_output, re.MULTILINE).group(1)
		except AttributeError:
			model = None
		# Get .network systemd unit
		try:
			network_file = re.match(r'.*Network File: (.*?)\\n', networkctl_output, re.MULTILINE).group(1)
		except AttributeError:
			network_file = None
		# Get state of interface
		try:
			state = re.match(r'.*State: (.*?)\\n', networkctl_output, re.MULTILINE).group(1)
			if 'unmanaged' in state:
				state = 'unmanaged'
			state = state.replace(' (configured)', '')
		except AttributeError:
			state = None

		# Get DHCP Status
		dhcp = False
		if network_file and os.path.exists(network_file):
			result, exit_code = utils.run_command('grep "^DHCP\\s*=\\s*ipv4$" {network_file}'.format(**locals()), hide_error=True)
			if exit_code == 0:
				dhcp = True

		if dhcp:
			# Get ip/netmask from ip output
			for address in interface['addr_info']:
				if address['family'] == 'inet':
					ip = address['local']
					netmask = utils.prefixlen_to_netmask(address['prefixlen'])
			try:
				gateway = re.match(r'.*Gateway: ([\d.]*)', networkctl_output, re.MULTILINE).group(1).strip()
			except AttributeError:
				gateway = None
			try:
				dns_raw = re.match(r'.*DNS: (.*)', networkctl_output, re.MULTILINE).group(1)
				dns_raw = dns_raw.split('\\n')
				for nameserver in dns_raw:
					nameserver = nameserver.strip()
					if utils.is_valid_ipv4_address(nameserver):
						dns.append(nameserver)
			except AttributeError:
				dns = list()
		else:
			# Get Config from network_file
			config_file = configparser.ConfigParser(strict=False)
			try:
				config_file.read(network_file)
			except (configparser.MissingSectionHeaderError, configparser.ParsingError) as e:
				error = {
					'code': 1,
					'message': {
						'de': 'Kann {network_file} nicht parsen: {e}'.format(**locals()),
						'en': 'Failed to parse {network_file}: {e}'.format(**locals()),
					}
				}
				return error, 500
			try:
				cidr = config_file['Network']['Address'].strip()
			except KeyError:
				cidr = None
			if cidr:
				ip, netmask = utils.cidr_to_subnetting(cidr)
				if not utils.is_valid_ipv4_address(ip):
					ip = None
			try:
				gateway = config_file['Network']['Gateway'].strip()
				if not utils.is_valid_ipv4_address(gateway):
					gateway = None
			except KeyError:
				gateway = None
			try:
				dns_raw = config_file['Network']['DNS'].strip()
				dns = dns_raw.split()
			except KeyError:
				dns = list()


		if name.startswith('en') or name.startswith('eth'):
			type = 'ethernet'
		else:
			continue

		try:
			mac = interface['address']
		except KeyError:
			mac = '00:00:00:00:00:00'

		config[name] = {
			'vendor': vendor,
			'model': model,
			'state': state,
			'dhcp': dhcp,
			'type': type,
			'mac': mac,
			'ip': ip,
			'netmask': netmask,
			'dns': dns,
			'gateway': gateway
		}

	return config, 200


def put(body):
	delete()
	for interface, settings in body.items():
		if settings.get('type', '') != 'ethernet':
			current_app.logger.info('Skipping Interface {interface}'.format(**locals()))
			continue
		current_app.logger.info('Configuring Interface {interface}'.format(**locals()))
		config = configparser.ConfigParser()
		config.optionxform = str

		# Match section
		config.add_section('Match')
		config.set('Match', 'Name', interface)

		# Network section
		config.add_section('Network')
		if settings.get('dhcp', True):
			config.set('Network', 'DHCP', 'ipv4')
			config.add_section('DHCP')
			config.set('DHCP', 'SendHostname', 'true')
			config.set('DHCP', 'UseHostname', 'true')
			config.set('DHCP', 'UseDomains', 'true')
		else:
			ip = settings.get('ip', None)
			if not utils.is_valid_ipv4_address(ip):
				current_app.logger.error('IP address invalid: {ip}'.format(**locals()))
				error = {
					'code': 1,
					'message': {
						'de': 'IP Adresse ungültig: {ip}'.format(**locals()),
						'en': 'IP address invalid: {ip}'.format(**locals())
					}
				}
				return error, 500

			# Address
			netmask = settings.get('netmask', None)
			if not netmask:
				current_app.logger.error('Netmask not set.'.format(**locals()))
				error = {
					'code': 1,
					'message': {
						'de': 'Netmask nicht gesetzt.',
						'en': 'Netmask not set.'
					}
				}
				return error, 500
			config.set('Network', 'Address', utils.subnetting_to_cidr(ip, netmask))

			# DNS
			dns = settings.get('dns', list())
			if dns:
				for nameserver in dns:
					nameserver = nameserver.strip()
					if not utils.is_valid_ipv4_address(nameserver):
						current_app.logger.error('DNS server IP address invalid: {ip}'.format(**locals()))
						error = {
							'code': 1,
							'message': {
								'de': 'DNS Server IP Adresse ungültig: {ip}'.format(**locals()),
								'en': 'DNS server IP address invalid: {ip}'.format(**locals())
							}
						}
						return error, 500
				config.set('Network', 'DNS', ' '.join(dns))

			# Gateway
			gateway = settings.get('gateway', None)
			if not utils.is_valid_ipv4_address(gateway):
				current_app.logger.error('Gateway IP address invalid: {ip}'.format(**locals()))
				error = {
					'code': 1,
					'message': {
						'de': 'Gateway IP Adresse ungültig: {ip}'.format(**locals()),
						'en': 'Gateway IP address invalid: {ip}'.format(**locals())
					}
				}
				return error, 500

			config.set('Network', 'Gateway', gateway)

		# Write config
		utils.run_command('mkdir -p /config/network')
		network_file_path = os.path.join('/config/network', '50-' + interface + '.network')
		with open(network_file_path, 'w') as network_file:
			config.write(network_file)

	try:
		utils.run_command('cp /config/network/* /etc/systemd/network/', raise_exception=True)
	except subprocess.CalledProcessError as e:
		current_app.logger.error('Failed to copy network config to systemd folder: {e}'.format(**locals()))
		error = {
			'code': 1,
			'message': {
				'de': 'Konnte Netzwerk-Einstellungen nicht aktivieren. Bitte starten Sie den Thinclient neu. ({e})'.format(**locals()),
				'en': 'Could not activate network settings. Please reboot. ({e})'.format(**locals())
			}
		}
		return error, 500

	try:
		utils.run_command('systemctl restart systemd-networkd', raise_exception=True)
	except subprocess.CalledProcessError as e:
		current_app.logger.error('Systemd-networkd failed.'.format(**locals()))
		error = {
			'code': 1,
			'message': {
				'de': 'Systemd-networkd ist gescheitert.',
				'en': 'Systemd-networkd failed.'
			}
		}
		return error, 500

	try:
		utils.run_command('systemctl restart systemd-resolved', raise_exception=True)
	except subprocess.CalledProcessError as e:
		current_app.logger.error('Systemd-resolved failed.'.format(**locals()))
		error = {
			'code': 1,
			'message': {
				'de': 'Systemd-resolved ist gescheitert.',
				'en': 'Systemd-resolved failed.'
			}
		}
		return error, 500
		
	# Reload IP Applet
	utils.run_command('sudo runuser -l $(getent passwd "1000" | cut -d: -f1) -c "DISPLAY=:0 /opt/tcos-libs/tcos/update_ip_applet.sh"')
		
	return NoContent, 204


def delete():
	temp_dir = tempfile.mkdtemp(prefix='network', dir='/tmp')
	# Backup current config to /tmp
	current_app.logger.info('Deleting configuration. Temporary backup in {temp_dir}.'.format(**locals()))
	utils.run_command('mv /config/network/50* ' + temp_dir, hide_error=True)
	utils.run_command('rm /etc/systemd/network/50* ', hide_error=True)

	return NoContent, 204

