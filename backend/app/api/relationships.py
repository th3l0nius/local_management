#!/usr/bin/env python

from app import utils


def search(id, flat=False):
    db = utils.get_db()
    table_shortitems = db['shortitems']
    table_relationships = db['relationships']

    query_filter = {
        'hardwaretype': 'device',
        'application_group': 'application',
        'location': 'printer'
    }

    # find all ids of related objects
    left_ids = table_relationships.find(left_id=id)
    right_ids = table_relationships.find(right_id=id)

    ids = [x['right_id'] for x in left_ids]
    ids += [x['left_id'] for x in right_ids]

    if not ids:
        return list(), 200

    items = table_shortitems.find(id=ids)

    result = []
    for idx, item in enumerate(items):
        if item['type'] in ['hardwaretype', 'application_group', 'location']:
            # find all ids of related objects
            left_ids = table_relationships.find(left_id=item['id'])
            right_ids = table_relationships.find(right_id=item['id'])

            ids = [x['right_id'] for x in left_ids]
            ids += [x['left_id'] for x in right_ids]
            sub_items = table_shortitems.find(id=ids, type=query_filter[item['type']])
            if flat:
                result = result + list(sub_items)
            else:
                item['relationships'] = list(sub_items)
        result.append(item)

    return result, 200


def put(id, body):
    db = utils.get_db()
    db.begin()
    try:
        table_relationships = db['relationships']
        table_relationships.delete(left_id=id)
        table_relationships.delete(right_id=id)

        for item in body:
            table_relationships.insert(dict(left_id=id, right_id=item['id']))

        db.commit()
    except Exception as e:
        db.rollback()
        raise e

    return search(id)
