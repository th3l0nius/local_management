#!/usr/bin/env python

from flask import current_app
import json

from app import utils
import app.api.relationships, app.api.item
from app import utils


def search(type='', subtype='', name='', mac_address='', with_defaults=True, allow_defaults_only=False, legacy_boolean=True):
	current_app.logger.error('Item filter type: "{type}", subtype: "{subtype}", name="{name}", mac="{mac_address}", ' \
							 'allow_defaults_only={allow_defaults_only}, with_defaults={with_defaults}'.format(**locals()))
	db = utils.get_db()
	table_shortitems = db['shortitems']

	# Get the ids of all related objects to the requested thinclient
	if mac_address:
		related_item_ids = list()
		if mac_address == '00:00:00:00:00:00':
			client_object = table_shortitems.find_one(type='client')
		else:
			client_object = table_shortitems.find_one(mac=mac_address)
		if client_object:
			client_id = client_object['id']
			related_item_ids.append(client_id)
			related_items, http_code = app.api.relationships.search(id=client_id, flat=True)
			for related_item in related_items:
				related_item_ids.append(related_item['id'])
		# If client wasn't found then only continue if we are interested in defaults only
		elif not client_object and not allow_defaults_only:
			return list(), 200

	# Create filter
	item_filter = dict()
	if type:
		item_filter['type'] = type
	if subtype:
		item_filter['subtype'] = subtype
	if name:
		item_filter['name'] = name
	if mac_address:
		item_filter['id'] = related_item_ids

	# Get shortitems from database
	shortitems = table_shortitems.all(**item_filter)

	# Load full items with settings
	result = list()
	for shortitem in shortitems:
		item, http_code = app.api.item.search(id=shortitem['id'], with_defaults=with_defaults, flattened_settings=True)
		item['settings'].pop('id', None)
		item['settings'].pop('item_id', None)
		result.append(item)

	# If no result was found and we are also interested in default only, then create an item with only the default settings
	if len(result) == 0 and allow_defaults_only and subtype != '*' and type != '*' and not name:
		# Get JSON schema
		response, http_code = app.api.schema.search(type=type, subtype=subtype)
		json_schema = json.loads(response.get_data(as_text=True))[0]
		# Get default settings
		default_settings = utils.get_defaults(json_schema)

		default_settings_model = dict(type=type, subtype=subtype, settings=default_settings)
		result.append(default_settings_model)

	current_app.logger.error('Number of results: {results}'.format(results=len(result)))

	# Replace None values with their default
	for item in result:
		# Get JSON schema
		response, http_code = app.api.schema.search(type=item['type'], subtype=item['subtype'])
		json_schema = json.loads(response.get_data(as_text=True))[0]
		# Get default settings
		default_settings = utils.get_defaults(json_schema)
		item['settings'] = utils.convert_none_to_default_value(item['settings'], default_settings)
		
	# Convert boolean values to string representations from XML schema
	if legacy_boolean:
		for item in result:
			item['settings'] = utils.convert_settings_to_legacy_boolean(type=item['type'],
																		subtype=item['subtype'],
																		settings=item['settings'])
	return result, 200
