#!/usr/bin/env bash

echo -e "*local_management*/webserver \t$(cat /proc/uptime | awk '{print $1}')" >> /var/log/boot_time.log
cd /opt/local_management
/usr/bin/python3 -c "from app import main; main.main()" &

echo "Wating for local management server to start"
until curl -Is http://localhost:5000 --max-time 1 | head -1 | grep OK
do
	sleep 0.1
done
echo "Local management server started"
systemd-notify --ready
echo -e "*local_management*/webserver \t$(cat /proc/uptime | awk '{print $1}')" >> /var/log/boot_time.log
wait
