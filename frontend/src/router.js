import Vue from 'vue'
import Router from 'vue-router'
import ThinClient from './views/ThinClient.vue'
import Items from './views/Items.vue'
import Item from './views/Item.vue'
import Files from './views/Files.vue'
import Network from './views/Network.vue'
import Display from './views/Display.vue'
import License from './views/License.vue'

Vue.use(Router)

const ItemType = (type) => {
  let route = {
    overview: `${type}s`,
    create: `${type}-create`,
    edit: type,
    duplicate: `${type}-duplicate`,
  }
  return [
    {
      path: `/${type}s`,
      name: route.overview,
      props: {
        type
      },
      meta: {
        section: type,
        route
      },
      component: Items
    },
    {
      path: `/${type}/duplicate/:id(\\d+)`,
      name: route.duplicate,
      props: route => ({
          id: route.params.id,
          duplicate: true
      }),
      meta: {
        section: type,
        route
      },
      component: Item
    },
    {
      path: `/${type}/create/:subtype`,
      name: route.create,
      props: route => ({
          type,
          subtype: route.params.subtype,
          create: true
      }),
      meta: {
        section: type,
        route
      },
      component: Item
    },
    {
      path: `/${type}/:id(\\d+)`,
      name: route.edit,
      props: true,
      meta: {
        section: type,
        route
      },
      component: Item
    }
  ]
}

export default new Router({
  mode: 'history',
  base: '/manager/',
  routes: [
    {
      path: '/thinclient',
      alias: '/',
      name: 'thinclient',
      meta: {
        section: 'thinclient'
      },
      component: ThinClient
    },
    ...ItemType('application'),
    ...ItemType('device'),
    {
      path: '/display',
      name: 'display',
      meta: {
        section: 'device'
      },
      component: Display
    },
    {
      path: '/files',
      name: 'files',
      meta: {
        section: 'files'
      },
      component: Files
    },
    {
      path: '/network',
      name: 'network',
      meta: {
        section: 'network'
      },
      component: Network
    },
    {
      path: '/license',
      name: 'license',
      meta: {
        section: 'license'
      },
      component: License
    },
  ]
})
