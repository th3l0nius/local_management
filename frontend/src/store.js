import Vue from 'vue'
import Vuex from 'vuex'
import api from './api.js'

Vue.use(Vuex)

{
  // dry_updater(key, fn) - cache function result as a Promise for STALE_TIME
  //
  // This is intented to allow repeated, unsynchronized update calls
  // without actually performing an API request each time.
  const STALE_TIME = 1000 // miliseconds
  let promises = Object.create(null)
  var dry_updater = (key, fn) => function(...args) {
    if(key in promises) {
      return promises[key]
    }
    return promises[key] = Promise.resolve(
        fn.apply(this, args)
      ).catch(ex => {
        delete promises[key]
        throw ex
      }).then( () =>
        setTimeout( () => { delete promises[key] }, STALE_TIME)
      )
  }
}

const to_shortitem = item => {
  let shortitem = Object.assign({}, item)
  delete shortitem.settings
  return shortitem
}

export default new Vuex.Store({
  state: {
    schemas: {},    // { type: {schema | subtype: {schema}} }
    shortitems: {}  // { type: [...items] }
  },
  getters: {
    types: state => Object.keys(state.schemas),
    subtypes: state => type => Object.keys(state.schemas[type])
  },
  mutations: {
    set_schemas(state, schemas) {
      state.schemas = schemas
    },
    set_shortitems(state, raw_shortitems) {
      let shortitems = Object.create(null)
      for(let item of raw_shortitems) {
        if(!(item.type in shortitems)) {
          shortitems[item.type] = []
        }
        shortitems[item.type].push(item)
      }
      state.shortitems = shortitems
    },
    set_select_shortitems(state, {type, items}) {
      state.shortitems = {...state.shortitems, [type]: items}
    },
    add_shortitem(state, item) {
      let items = state.shortitems[item.type]
      if(items) {
        items.push(item)
      } else {
        state.shortitems[item.type] = [item]
      }
    },
    update_shortitem(state, item) {
      let items = state.shortitems[item.type]
      if(items) {
        let i = items.findIndex( ({id}) => item.id == id )
        if(i > -1) {
          items.splice(i, 1, item)
        }
      }
    },
    delete_shortitem(state, item) {
      let items = state.shortitems[item.type]
      if(items) {
        let i = items.findIndex( ({id}) => item.id == id )
        if(i > -1) {
          items.splice(i, 1)
        }
      }
    }
  },
  actions: {
    update_schemas: dry_updater('update_schemas', async (context, i18n) =>
      context.commit('set_schemas', await api.load_schemas(i18n))
    ),
    update_shortitems: dry_updater('update_shortitems',
      async context =>
          context.commit('set_shortitems', await api.load_shortitems())
    ),
    async update_select_shortitems(context, type, subtype) {
      await context.commit('set_select_shortitems', {
        type, items: await api.load_shortitems(type, subtype)
      })
    },
    delete_item(context, item) {
      context.commit('delete_shortitem', item)
      return api.delete_item(item.id).catch( ex => {
          if(!(ex instanceof api.NotFoundError)) {
            // eslint-disable-next-line no-console
            console.error('Error while deleting item', ex)
            context.commit('add_shortitem', to_shortitem(item))
            throw ex
          }
      })
    },
    save_item(context, item) {
      if(item.id) {
        return api.save_item(item).then(() =>
          context.commit('update_shortitem', to_shortitem(item))
        )
      } else {
        return api.save_item(item).then(
          () => context.commit('add_shortitem', to_shortitem(item)),
          ex => { throw ex }
        )
      }
    }
  }
})
