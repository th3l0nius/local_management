export default {
  props: {
    name: String,
    value: String,
    disabled: {
      type: Boolean,
      default: false
    },
    schema: {
      type: Object,
      default: Object
    },
    i18n_path: String,
    level: Number
  },
  computed: {
    label() {
      let key = `${this.i18n_path}.label`
      return this.$i18n.te(key)? this.$i18n.t(key) : ''
    },
    valid() {
      return true
    }
  },
  filters: {
    capitalize: function (value) {
      if (!value) return ''
      value = value.toString()
      return value.charAt(0).toUpperCase() + value.slice(1)
    }
  },
  methods: {
    validate(){},
    focus(last = false) { this.$emit(last? "focus_prev" : "focus_next") },
    focus_first() { this.focus() },
    focus_last() { this.focus(true) },
    focus_prev() { this.$emit("focus_prev") },
    focus_next() { this.$emit("focus_next") }
  }
}
