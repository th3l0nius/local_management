import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import i18n from './i18n'
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css'
import 'focus-visible'
import {version} from '../package.json'

Vue.config.productionTip = false
Vue.use(i18n)

vSelect.props.components.default = () => ({
  Deselect: {
    render: () => null
  },
  OpenIndicator: {
    render: createElement => createElement('span')
  },
})
Vue.component('v-select', vSelect)

Vue.mixin({
  methods: {
    localeCompare(key) {
      return (l, r) => this.$root.collator.compare(l[key], r[key])
    }
  }
})

window.app = new Vue({
  data() {
    return {
      collator: new Intl.Collator(this.$i18n && this.$i18n.locale || 'en'),
      version
    }
  },
  mounted() {
    document.documentElement.lang = this.$i18n && this.$i18n.locale || 'en'
  },
  watch: {
    '$i18n.locale'(locale) {
      this.collator = new Intl.Collator(locale)
      document.documentElement.lang = locale
    }
  },
  router,
  i18n,
  store,
  render: h => h(App)
}).$mount('#openthinclient')
