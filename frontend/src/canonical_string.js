/*
  Produce a canonical string representation of given object.
  This is meant to facilitate easy (string-based) equality tests of complex
  objects. The result is NOT a serialisation.

  Entries with nully or empty values are removed/ignored from arrays or objects.
  If the given object is / ends up nully or empty, the empty string is returned.
  (Non-array) objects are mapped to their sorted entries array.

  Supported objects are those that are supportetd by JSON.
  (I.e. Map, Set or other complex objects won't work as expected.)
*/

const IGNORE_KEYS = ['id', 'item_id']
const NULL_VALUES = [undefined, null, NaN, '']

const denull = o => {
  if(NULL_VALUES.includes(o)) {
    return undefined
  } else if(typeof o != 'object') {
    return o
  } else if(Array.isArray(o)) {
      o = o.map(denull).filter(o => o !== undefined)
      return o.length? o : undefined
  } else {
    let e = []
    for(let [k, v] of Object.entries(o)) {
      if(!IGNORE_KEYS.includes(k)) {
        v = denull(v)
        if(v !== undefined) {
          e.push([k, v])
        }
      }
    }
    return e.length? e.sort() : undefined
  }
}

export default obj => {
  obj = denull(obj)
  return obj === undefined? '' : JSON.stringify(obj)
}
